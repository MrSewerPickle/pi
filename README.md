# README #

This is a collection of scripts and procedures I use on the RaspberryPI for management of various 
PI instances (ranging from Pi1 -> Pi3 B+).

### What is this repository for? ###

* Quick summary
Shell scripts and other management tools to keep all of your RaspberryPIs up-to-date and manageable.
Also, steps for creating a headless raspberry pi instance with minimal sdcard space usage

* Version
N/A (for now)

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

## Summary of Using Scripts
1. Clone the repo
2. Execute necessary scripts. The names should be pretty self explanitory. The accounts should have root sudo access

## Summary of Creating a Image from Scratch
1. Download Rasbian Lite - Unzip it.
2. Win32DiskImager - Write to SDCard (16+ GB SanDisk Ultra Plus+ recommended, may need Micro to SD Adapter for Windows to pick it up)
3. Create a empty ssh (no extension) file in the root partition of the sd card.
3. Boot GPart 0.30.0+ (probably from SDCARD or Disk)
5. Format to (16 GB Card / 32 GB Card):

   - F:boot    FAT16 60.00 MB    PRIMARY (up to 90)
   - G:        OTHER  4.88 GB    PRIMARY
   - H:server  EXT3  11.77 GB    LOGICAL (up to 23 on 32GB SD)

5. Boot Raspberry PI
6. SSH Login as default pi (u: pi, p: raspberry)
7. passwd to another password.
8. Copy scripts in /server/tools

## Summary of Creating a Headless Server Standard
1. Download Rasbian Lite - Unzip it.
2. Mount sdcard with 32 GB card in Windows
3. Use Etcher to write the Lite img to the sdcard
4. Mount the drive again on G:\boot
5. Save a empty file named `ssh` in G:\boot
6. Unmount the SDCard in Windows, put the card in the Raspberry PI and 
   boot it with a network cable attached.
7. Determine DHCP IP for new Raspberry PI and ssh to it on SSH defaults.
8. SSH Login as default pi (u: pi, p: raspberry)
9. Run `sudo raspi-config`
10. Select Expanded Storage Option.
11. Save and reboot the Raspberry Pi
12. Boot on a GPart 3.0+ DVD on the PC.
13. Select all defaults when booting.
14. In GPart via the booted DVD, select "rootfs" or "root" and select resize.
    + Set new size to 4801
	+ You should have around 25588 free.
	+ Save Settings / Click OK.
15. Select the newly created unused area below "rootfs" or "root"
16. Right-Click on the unused selection and select Create a new Partition
17. Make it a Primary Partition.
    + Size : 25588 (or total left over)
	+ Label: server
18. Apply GPart settings. 
19. Reboot and remove the GPart DVD.
20. Move the SDcard Image to the Raspberry PI 
21. Boot the Raspberry PI with network cable attached.
22. SSH Login as default pi (u: pi, p: raspberry)
23. Run `sudo fdisk -l` and take note of the items listed at the bottom.

	### Example 
```
	Disk /dev/mmcblk0: 29.7 GiB, 31914983424 bytes, 62333952 sectors
	Units: sectors of 1 * 512 = 512 bytes
	Sector size (logical/physical): 512 bytes / 512 bytes
	I/O size (minimum/optimal): 512 bytes / 512 bytes
	Disklabel type: dos
	Disk identifier: 0xde4f906a

	Device         Boot   Start      End  Sectors  Size Id Type
	/dev/mmcblk0p1         8192    96663    88472 43.2M  c W95 FAT32 (LBA)
	/dev/mmcblk0p2        98304  9930751  9832448  4.7G 83 Linux
	/dev/mmcblk0p3      9930752 62332927 52402176   25G 83 Linux
```

24. Run `sudo vi /etc/fstab` and make sure the entries match, if they don't make changes
	to match the EXACT /dev/ define listed in fdisk output above.

	### Example
```
	proc            /proc           proc    defaults          0       0
	/dev/mmcblk0p1  /boot           vfat    defaults          0       2
	/dev/mmcblk0p2  /               ext4    defaults,noatime  0       1
	/dev/mmcblk0p3  /server         ext4    defaults,noatime  0       1
	# a swapfile is not a swap partition, no line here
```

25. Run `sudo vi /boot/cmdline.txt` and make sure the root entry matches the / entry in the
    previous step above. If they don't match, update it as well.
	
	### Example
```
	dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait`
```

26. Run `sudo vi /etc/hosts` and update the hostname of the server to a different name
27. Run `sudo vi /etc/hostname` and update the hostname of the server to match previous step.
28. Run `sudo passwd` to change the pi password from the default listed above to a better one.
29. Run `sudo shutdown -r now` to reboot the Raspberry PI
30. SSH Login as default pi (u: pi, p: new pwd).
31. SSH Login to another Raspberry PI server or Winscp the Pi Repo tools over from a Windows box.
	
	### Example copying from existing server to new server (recursive)
```
	sudo scp -rp pi@192.168.1.122:/server/tools /server/tools`
```

32. Create symlinks in ~ for pi

	- Run `ln -s /server /home/pi/server`
	- Run `ln -s /etc/apt/apt.conf.d apt.conf.d`

33. Update the pi .bashrc for shortcuts

	- Run `vi .bashrc` to edit and add alias
```
	alias ll='ls -la'
	alias la='ls -A'
	alias l='ls -CF'
	alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
	alias psg='ps -elf | grep -v grep | grep'
```

## Dependencies
- https://downloads.raspberrypi.org/raspbian_lite_latest - (NO Desktop, minimal)
- https://www.easeus.com/download/epmf-download.html - Creating partitions
- https://sourceforge.net/projects/win32diskimager/files/latest/download - Writing Images to SDCard
- https://etcher.io/ - Best UI for Writing Raspberry PI Images