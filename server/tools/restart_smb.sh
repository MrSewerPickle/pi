#!/bin/sh

echo "Restarting SAMBA..."
sudo service smbd restart

echo "Restarting NMBD Service..."
sudo service nmbd restart
