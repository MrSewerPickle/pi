sudo apt-get install sshfs
sudo modprobe fuse
sudo adduser pi fuse
sudo chown root:fuse /dev/fuse
sudo chmod +x /dev/fusermount

// Create Dir //
mkdir /server/sprinklerpi
mkdir /server/speedpi
mkdir /server/securitypi
mkdir /server/threepi

// Remote Mount Command //
sshfs pi@192.168.1.114.:/server/ /server/sprinklerpi
sshfs pi@192.168.1.122.:/server/ /server/speedpi
sshfs pi@192.168.1.119.:/server/ /server/securitypi
sshfs pi@192.168.1.126.:/server/ /server/threepi
