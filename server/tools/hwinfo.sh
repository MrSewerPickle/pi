#!/bin/sh

echo "CPU Info...."
/usr/bin/lscpu
echo ""

echo "Memory Info...."
/usr/bin/vcgencmd get_mem arm && vcgencmd get_mem gpu
echo ""

echo "Memory left for usage...."
/usr/bin/free -o -h
echo ""

echo "Disk / FileSystem Info...."
/bin/df -hT
echo ""

