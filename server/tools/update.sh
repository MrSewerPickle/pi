#!/bin/sh

echo "Making sure rpi-update is installed..."
sudo apt-get install rpi-update
echo "Updating firmware and kernel..."
sudo rpi-update


echo "Updating apt-get repo..."
sudo apt-get update
echo "Applying newly updated apt-get repo..."
sudo apt-get upgrade

