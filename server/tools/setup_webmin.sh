#!/bin/sh

echo "Checking if webmin APT instance is there..."
COUNT=`sudo grep -c webmin /etc/apt/sources.list`
if [ "$COUNT" = 0 ]; then 
	echo "-> $COUNT entries found...install the APT instance"
	sudo bash -c 'echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list'
else 
	echo "-> webmin apt source already set"
fi

echo "Checking to see if GPG key is installed..."
sudo wget http://www.webmin.com/jcameron-key.asc
sudo apt-key add jcameron-key.asc

echo "Updating APT repo..."
sudo apt-get -y update
echo "Installing webmin..."
sudo apt-get -y install webmin

echo "Removing temp GPG key that was downloaded..."
sudo rm ./jcameron-key.asc

echo "Creating necessary directories in /server"
sudo mkdir /server/webmin
sudo chgrp pi /server/webmin
sudo chown pi /server/webmin

echo "Copying newly installed webmin to /server"
sudo cp -rf /etc/webmin /server/webmin/app
sudo cp -rf /var/webmin /server/webmin/var

echo "Stopping webmin server for moving..."
sudo /etc/webmin/stop

echo "Removing old webmin in /etc..."
sudo rm -rf /etc/webmin

echo "Creating symlink to /server..."
cd /etc
sudo ln -s /server/webmin/app webmin

echo "Removing old webmin in /var..."
sudo rm -rf /var/webmin

echo "Creating symlink to /server for webmin /var..."
cd /var
sudo ln -s /server/webmin/var webmin

echo "Starting webmin again on https://localhost:10000"
sudo /etc/webmin/start

echo "Go to the URL listed above and just login with your current userid"
